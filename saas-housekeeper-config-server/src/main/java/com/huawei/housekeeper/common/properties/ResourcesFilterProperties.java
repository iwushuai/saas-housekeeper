
package com.huawei.housekeeper.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 功能描述
 *
 * @since 2022-07-05
 */
@ConfigurationProperties(prefix = "spring.security.filters")
@RefreshScope
public class ResourcesFilterProperties {
    private String[] includeUris;

    public String[] getIncludeUris() {
        return includeUris;
    }

    public void setIncludeUris(String[] includeUris) {
        this.includeUris = includeUris;
    }
}