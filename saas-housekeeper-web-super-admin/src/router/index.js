const router = [
    {
        path: '/',
        name: '/',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/login',
        name: '/login',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/home',
        name: '/home',
        component: () => import('@/views/Home/Home.vue'),
        children: [
            {
                path: '/home/service-list',
                name: '/home/service-list',
                component: () => import('@/views/ServiceList/ServiceList.vue'),
            },
        ],
    },
];

export default router;
