import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import ElementPlus from 'element-plus';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import selfRoutes from './router';
import App from './App.vue';
import '@/assets/style/default.css';
import 'element-plus/dist/index.css';

const router = createRouter({
    history: createWebHashHistory(),
    routes: selfRoutes,
});

const app = createApp(App);
app.use(ElementPlus, {
    locale: zhCn,
});
app.use(router);
router.beforeEach((guard) => {
    const { matched } = guard;
    if (!matched.length) {
        router.push({
            name: '/home/service-list',
        });
    }
});

app.use(ElementPlus);
app.mount('#app');
