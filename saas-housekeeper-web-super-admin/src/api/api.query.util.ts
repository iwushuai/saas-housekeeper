import { AxiosPromise } from 'axios';
import axios from './api.request';

interface Response {
    code: number;
    result: any;
    message: string;
}

export function queryFn(url: string, params: any): Promise<Response> {
    return axios.request({
        url,
        method: params.method,
        data: params.data,
    });
}

/**
 * 登录接口
 * @param params
 */
export function userLogin(params: { method: string; data: {} }) {
    return queryFn('/admin/login', params);
}

/**
 * 获取租户列表
 * @param params
 */
export function getTenants(params: { method: string; data: {} }) {
    return queryFn('/admin/tenants', params);
}

/**
 * 对租户注册，注销
 * @param params
 */
export function setTenant(params: { method: string; data: {} }) {
    return queryFn('/admin/tenant', params);
}
