import { ref, Ref, watch } from 'vue';
import { useRouter, useRoute, RouteRecordName } from 'vue-router';
// @ts-ignore
import homeIcon from '../../assets/image/home@2x.png';

// 菜单配置
export const asideList = ref([
    {
        key: '/home/service-list', // 填写路由名称，菜单关联路由
        value: '租户管理',
        icon: homeIcon,
    },
]);

export const routeKey: Ref<RouteRecordName> = ref('');
export const showActiveKey: Ref<RouteRecordName> = ref('');

export function handleSelect(key: string) {
    routeKey.value = key;
}

export function init() {
    const router = useRouter();
    const route = useRoute();

    watch(routeKey, (val, oVal) => {
        if (val !== oVal && asideList.value.some((e) => e.key === val)) {
            router.push({ name: val });
        }
    });

    watch(
        route,
        (val) => {
            let activeMenuKey = '';
            if (typeof val.name === 'string') {
                activeMenuKey = val.name.match(/^\/.+\/.+\//)?.[0].replace(/\/$/, '') || val.name;
            }

            showActiveKey.value = activeMenuKey;
            routeKey.value = val?.name ?? '';
        },
        {
            immediate: true,
        },
    );
}
