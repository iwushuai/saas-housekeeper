module.exports = {
    publicPath: './',
    outputDir: 'dist',
    assetsDir: 'static',
    filenameHashing: true,
    lintOnSave: false,
    devServer: {
        hot: true,
        port: 8083,
        compress: true,
        allowedHosts: 'all',
    },
    configureWebpack: {},
};
