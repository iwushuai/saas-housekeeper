FROM nginx:alpine-perl
COPY html /usr/share/nginx/html
COPY conf /etc/nginx
RUN mkdir /etc/nginx/logs/
RUN chmod -R 777 /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80

