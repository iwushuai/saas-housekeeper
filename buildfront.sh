#!/bin/bash
rm -rf html
mkdir html
npm install -g cnpm --registry=https://registry.npm.taobao.org
cd saas-housekeeper-web-customer
cnpm install
rm -rf package-lock.json
cnpm run build
cd ..
mv saas-housekeeper-web-customer/dist html/customer

cd saas-housekeeper-web-super-admin
cnpm install||cnpm install --save core-js
rm -rf package-lock.json
cnpm run build
cd ..
mv saas-housekeeper-web-super-admin/dist html/super-admin

cd saas-housekeeper-web-tenant
cnpm install
rm -rf package-lock.json
cnpm run build
cd ..
mv saas-housekeeper-web-tenant/dist html/tenant

cd saas-housekeeper-web-worker
cnpm install
rm -rf package-lock.json
cnpm run build
cd ..
mv saas-housekeeper-web-worker/dist html/worker
