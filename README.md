
[TOC]

**项目介绍**
===============
本项目是华为云开发者团队基于SaaS项目技术支持实践，采用微服务架构，结合华为云服务能力开发的SaaS化开源项目，旨在为企业级开发者提供云原生SaaS应用构建的技术参考，包括微服务架构、多租隔离设计、多租户路由、数据存储多租设计等。更多SaaS相关技术细节可参考：华为云开发者文档中心SaaS应用开发指导。

![saas-housekeeper.PNG](document/jdk-1.8-brightgreen.svg) ![saas-housekeeper.PNG](document/maven-3.6.3-red.svg)

**架构图**
---------------
![saas-housekeeper.PNG](document/业务架构图.png)
![saas-housekeeper.PNG](document/技术架构图.png)

**组件图**

![](document/组件图.png)

**项目演示**
---------------

- 访问地址

| 名称  | 地址  | 账号 |
| ------------ | ------------ | ------------ |
| 租户注册      | [http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/register](http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/register) | 不需要 |
|超级管理中心   | [http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/super-admin/#/login](http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/super-admin/#/login) | 账号:  Admin </br> 密码:   lhKk101@mm.  |
| 租户管理中心  | [http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/login](http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/login) | 账号:  tenant </br> 密码:   lhKk101@mm.  |
| 用户管理中心  | [http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/customer/#/login](http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/customer/#/login) | 需注册 |
| 工人管理中心  | [http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/worker/#/login](http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/worker/#/login) | 需注册 |

​                       NOTE:  五级域名*“sample1”*为租户定义，可替换为租户自己申请的域名，注册域名时请注意五级域名不包含“.” 。不能注册为“sample1.com”



- 体验流程



步骤一：租户（企业）通过访问租户管理中心-租户注册(http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/register),进行租户注册操作

管理员可通过访问租户管理中心(http://saas-housekeeper.cloudbu.cloud-onlinelab.cn/super-admin/#/login) 对提交的租户注册请求进行审批

ps：租户域名为租户注册时填写的栏目，为后续租户、用户、工人中心网站分配动态url

步骤二：注册审批通过的租户可通过访问发布中心([http://(租户域名).saas-housekeeper.cloudbu.cloud-onlinelab.cn/tenant/#/login](#))定制企业服务

步骤三：最终用户可通过访问用户管理中心([http://(租户域名).saas-housekeeper.cloudbu.cloud-onlinelab.cn/customer/#/login)][http://(租户域名).saas-housekeeper.cloudbu.cloud-onlinelab.cn/worker/#/login]对租户提供的定制服务进行购买下单

步骤四：雇员可以通过访问工人管理中心([http://(租户域名).saas-housekeeper.cloudbu.cloud-onlinelab.cn/worker/#/login](#))接取最终用户的订单



- 时序图



![](document/家政SAAS序列图.png)



- 租户注册,管理员审批
  ![saas-housekeeper.PNG](document/租户注册审批.gif)
- 租户新建服务
  ![saas-housekeeper.PNG](document/租户新建服务.gif)
- 用户下单
  ![saas-housekeeper.PNG](document/用户下单.gif)
- 工人接单
  ![saas-housekeeper.PNG](document/工人接单.gif)

**组织结构**
---------------

``` lua
saas-housekeeper
├── saas-housekeeper-common-utils -- 工具类及通用代码
├── saas-housekeeper-config-server -- 配置中心
├── saas-housekeeper-eureka -- 注册中心
├── saas-housekeeper-gateway -- 网关中心
├── saas-housekeeper-message-service -- 消息中心
├── saas-housekeeper-order-service -- 订单中心
├── saas-housekeeper-service-publish-service -- 服务发布中心
├── saas-housekeeper-web-customer -- 用户管理中心
├── saas-housekeeper-web-super-admin -- 超级管理员管理中心
├── saas-housekeeper-web-tenant -- 租户管理中心
├── saas-housekeeper-web-worker -- 任务管理中心
└── saas-housekeeper-config -- 配置文件
```


**技术选型**
---------------
| 技术                  | 说明                | 官网                                          |
| -------------------- | -------------------| --------------------------------------------  |
| Spring-Cloud         | 微服务框架           | https://spring.io/projects/spring-cloud       |
| SpringBoot           | 容器+MVC框架         | https://spring.io/projects/spring-boot        |
| Eureka               | 注册中心             | https://github.com/xmartlabs/Eureka           |
| SpringSecurity       | 认证和授权框架        | https://spring.io/projects/spring-security    |
| MyBatis-plus         | ORM框架             | https://baomidou.com/                         |
| K8S                  | 华为云应用容器引擎CCE  | https://support.huaweicloud.com/cce/index.html |
| Mysql                | 云数据库RDS          | https://support.huaweicloud.com/rds/index.html |
| Redis                | 分布式缓存DCS服务     | https://support.huaweicloud.com/intl/zh-cn/dcs/index.html |
| RabbitMQ             | 分布式消息队列 DMS    | https://support.huaweicloud.com/intl/zh-cn/rabbitmq/index.html |
|  saas-tenant-router-starter               | 多租户路由中间件           | https://gitee.com/HuaweiCloudDeveloper/saas-tenant-router-starter.git |
| JWT                  | JWT登录支持          | https://github.com/jwtk/jjwt                   |
| Lombok               | 简化对象封装工具      | https://github.com/rzwitserloot/lombok         |
| Swagger-UI           | 文档生成工具         | https://github.com/swagger-api/swagger-ui      |


**项目启动**
---------------

- 安装配置RabbitMQ
  - 登录RabbitMQ，配置Vitual host为saas-houskeeper
  - 增加exchanges名称为amq.topic，增加queues绑定amq.topic配置如下:

    |Name|Routing Key|
    |-|-|
    |message.queue|message.#|
    |order.task|task.order.newOrder|
    |task.order|order.task.doTask|

- 执行/db目录下脚本初始化数据库
- 后端服务启动注册中心eureka和配置中心config-server,其它子模块启动顺序任意
- 前端服务启动模块:
  - saas-housekeeper-web-customer
  - saas-housekeeper-web-super-admin
  - saas-housekeeper-web-tenant
  - saas-housekeeper-web-worker

- 在目录下执行命令
  > npm install
  >
  > npm run serve
- 本地访问地址
  - 以租户域名abc为例,修改host文件,增加下列配置,实现域名映射:
  > 127.0.0.1 abc.saas-housekeeper.cloudbu.huawei.com
  -  超级管理中心: [abc.saas-housekeeper.cloudbu.huawei.com:8083/#/login](abc.saas-housekeeper.cloudbu.huawei.com:8083/#/login)
  -  租户管理中心: [abc.saas-housekeeper.cloudbu.huawei.com:8082/#/login](abc.saas-housekeeper.cloudbu.huawei.com:8082/#/login)
  -  用户管理中心: [abc.saas-housekeeper.cloudbu.huawei.com:8081/#/login](abc.saas-housekeeper.cloudbu.huawei.com:8081/#/login)
  -  工人管理中心: [abc.saas-housekeeper.cloudbu.huawei.com:8084/#/login](abc.saas-housekeeper.cloudbu.huawei.com:8084/#/login)
  - 注意：目前区分租户是采用5级域名区分的,所以域名格式必须为  abc.XXX.XXX.XXX.XXX 其中abc为租户标识,作为租户标识,用于区分不同租户


**相关参考**
---------------
- 华为云开发者中心SaaS应用开发指导
- [Istio版本](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-istio-k8s-saas-housekeeper)
- [多租路由中间件开源项目](https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper/blob/master-dev/saas-housekeeper-tenant-router-starter/README.md)
