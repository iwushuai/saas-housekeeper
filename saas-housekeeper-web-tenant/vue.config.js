const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    publicPath: './',
    outputDir: 'dist',
    assetsDir: 'static',
    filenameHashing: true,
    lintOnSave: false,
    devServer: {
        hot: true,
        port: 8080,
        allowedHosts: 'all',
    },
    // 自定义webpack配置
    configureWebpack: {},
};
