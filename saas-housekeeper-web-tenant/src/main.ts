import 'element-plus/dist/index.css';
import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import ElementPlus from 'element-plus';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import { routerGuard, directiveLog } from '@/utils/utils';
import selfRoutes from './router';
import App from './App.vue';
import './assets/style/default.css';

const router = createRouter({
    history: createWebHashHistory(),
    routes: selfRoutes,
});

const app = createApp(App);
app.use(ElementPlus, {
    locale: zhCn,
});
app.use(router);
routerGuard(router);
directiveLog(app);
app.use(ElementPlus);
app.mount('#app');
