import { axios } from './api.request';

export function queryFn(url: string, params: any) {
    return axios.request({
        url,
        method: params.method,
        data: params.data,
    });
}

/**
 * 登录接口
 * @param params
 */
export function userLogin(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/login/user-login', params);
}

/**
 * 新增服务 POST
 * @param params
 */
export function createService(params: any) {
    return queryFn('/saas-public/housekeeper/createService', params);
}

/**
 * 更新服务 POST
 * @param params
 */
export function updateService(params: any) {
    return queryFn('/saas-public/housekeeper/updateService', params);
}

/**
 * 删除服务 POST
 * @param params
 */
export function deleteService(params: any) {
    return queryFn('/saas-public/housekeeper/deleteService', params);
}

/**
 * 新增规格 POST
 * @param params
 */
export function createServiceSpecification(params: any) {
    return queryFn('/saas-public/housekeeper/createServiceSpecification', params);
}

/**
 * 新增规格配置价格 POST
 * @param params
 */
export function createServiceSelection(params: any) {
    return queryFn('/saas-public/housekeeper/createServiceSelection', params);
}

/**
 * 用specId删除规格 delete
 * @param params
 */
export function deleteSpecification(params: any) {
    return queryFn('/saas-public/housekeeper/deleteSpecification', params);
}

/**
 * 用skuId删除规格配置价格 delete
 * @param params
 */
export function deleteServiceSkuBySkuId(params: any) {
    return queryFn('/saas-public/housekeeper/deleteServiceSkuBySkuId', params);
}

/**
 * 查询服务列表 POST
 * @param params
 */
export function getServiceList(params: any) {
    return queryFn('/saas-public/housekeeper/getServiceList', params);
}

/**
 * 查询单个服务详情 POST
 * @param params
 */
export function getServiceById(params: any) {
    return queryFn(`/saas-public/housekeeper/getServiceById?serviceId=${params.data.serviceId}`, params);
}

/**
 * 用skuId编辑规格配置价格 POST
 * @param params
 */
export function updateServiceSelection(params: any) {
    return queryFn('/saas-public/housekeeper/updateServiceSelection', params);
}

/**
 * 获取所有消息 GET
 * @param params
 */
export function getAllMsg(params: any) {
    return queryFn('/saas-message/msg/all-msg', params);
}

/**
 * 获取已读/未读消息 GET
 * @param params
 */
export function getMsg(params: any) {
    return queryFn(`/saas-message/msg/msg/${params.data.status}`, params);
}

/**
 * 获取未读消息数量 GET
 * @param params
 */
export function getCount(params: any) {
    return queryFn('/saas-message/msg/count', params);
}

/**
 * 将消息标为已读 POST
 * @param params
 */
export function setMsg(params: any) {
    return queryFn('/saas-message/msg/state', params);
}

/**
 * 删除已读消息 POST
 * @param params
 */
export function deleteMsg(params: any) {
    return queryFn('/saas-message/msg/del', params);
}

/**
 * 租户添加主题配置 POST
 * @param params
 */
export function createTenantStyleCustomization(params: any) {
    return queryFn('/saas-public/styleCustomization/createTenantStyleCustomization', params);
}

/**
 * 租户查询主题配置 POST
 * @param params
 */
export function getTenantStyleCustomization(params: any) {
    return queryFn('/saas-public/styleCustomization/getTenantStyleCustomization', params);
}

/**
 * 租户查询订单列表 POST
 * @param params
 */
export function getTenantOrders(params: any) {
    return queryFn('/saas-order/order/tenant-orders', params);
}

/**
 * 根据订单id查询订单 GET
 * @param params
 */
export function getOrderDetails(params: any) {
    return queryFn(`/saas-order/order/order/orderNumber/${params.data.orderNumber}`, params);
}

/**
 * 用户创建订单 POST
 * @param params
 */
export function createOrder(params: any) {
    return queryFn('/saas-order/order/order', params);
}

/**
 * 创建用户信息/用户注册 POST 不走网关 直接调tenantinfo-service端口为8500
 * @param params
 */
export function registerApi(params: { method: string; data: {} }) {
    return queryFn('/tenant/tenant', params);
}
