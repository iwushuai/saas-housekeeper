const router = [
    {
        path: '/login',
        name: '/login',
        component: () => import('../views/Login/Index.vue'),
    },
    {
        path: '/change-theme',
        name: '/change-theme',
        component: () => import('../views/FeaturedThemes/Index.vue'),
    },
    {
        path: '/register',
        name: '/register',
        component: () => import('@/views/Register/Register.vue'),
    },
    {
        path: '/success',
        name: '/success',
        component: () => import('@/views/Register/SubmitSuccess.vue'),
    },
    {
        path: '/home',
        name: '/home',
        component: () => import('../views/Home/Index.vue'),
        children: [
            {
                path: '/home/service-management',
                name: '/home/service-management',
                component: () => import('../views/ServiceManagement/Index.vue'),
            },
            {
                path: '/home/service-management/modify',
                name: '/home/service-management/modify',
                component: () => import('../views/ServiceManagement/CreateService.vue'),
            },
            {
                path: '/home/orders-management',
                name: '/home/orders-management',
                component: () => import('../views/OrderManagement/Index.vue'),
            },
            {
                path: '/home/orders-management/Details',
                name: '/home/orders-management/Details',
                component: () => import('../views/OrderManagement/OrderDetails.vue'),
            },
            {
                path: '/home/worker-management',
                name: '/home/worker-management',
                component: () => import('../views/WorkerManagement/Index.vue'),
            },
            {
                path: '/home/customer-management',
                name: '/home/customer-management',
                component: () => import('../views/CustomerManagement/Index.vue'),
            },
        ],
    },
];

export default router;
