/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.housekeeper.common.constant.Constant;
import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.controller.request.DelMsgDto;
import com.huawei.housekeeper.controller.request.ReadMsgDto;
import com.huawei.housekeeper.dao.entity.Message;
import com.huawei.housekeeper.service.MessageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * controller层
 *
 * @since 2022-02-15
 */
@RefreshScope
@RestController
@Validated
@RequestMapping("/msg")
@Api(tags = "获取消息")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping(value = "/all-msg")
    @ApiOperation(value = "获取全部消息")
    public Result<List<Message>> getAllMsg() {
        return Result.createResult(messageService.getAllMsg());
    }

    @GetMapping(value = "/msg/{status}")
    @ApiOperation(value = "获取未读或已读消息")
    public Result<List<Message>> getMsg(@PathVariable("status") Integer status) {
        return Result.createResult(messageService.getMsg(status));
    }

    @GetMapping(value = "/count")
    @ApiOperation(value = "获取未读消息总数")
    public Result<Integer> getCount() {
        return Result.createResult(messageService.getMsgCount());
    }

    @PostMapping(value = "/state")
    @ApiOperation(value = "将消息置为已读")
    public Result<Boolean> setMsg(@Valid @RequestBody ReadMsgDto readMsgDto) {
        return Result.createResult(messageService.setMsgStatus(readMsgDto.getUserIdList(), Constant.MessageStatus.READ_ED));
    }

    @PostMapping(value = "/del")
    @ApiOperation(value = "删除已读消息")
    public Result<Boolean> deleteMsg(@Valid @RequestBody DelMsgDto delMsgDto) {
        return Result.createResult(messageService.setMsgStatus(delMsgDto.getUserIdList(), Constant.MessageStatus.DEL));
    }
}
