/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.controller.request.UserLoginDto;
import com.huawei.housekeeper.service.UserLoginService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户登录
 *
 * @author y00464350
 * @since 2022-02-14
 */
@RestController
@Validated
@RequestMapping("/login")
@Api(tags = "用户登录")
public class LoginController {
    @Autowired
    private UserLoginService userLoginService;

    @PostMapping(value = "/user-login")
    @ApiOperation(value = "用户登录")
    public Result<String> userLogin(@Valid @RequestBody UserLoginDto userLoginDto,
                                    @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userLoginService.userLogin(userLoginDto));
    }
}
