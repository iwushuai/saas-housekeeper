/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.housekeeper.commonutils.result.ListRes;
import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.controller.request.CreateUserDto;
import com.huawei.housekeeper.controller.request.GetUserDto;
import com.huawei.housekeeper.controller.request.GetWorkUserInfoDto;
import com.huawei.housekeeper.controller.request.PageQueryUserDto;
import com.huawei.housekeeper.controller.response.GetTenantUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserVo;
import com.huawei.housekeeper.controller.response.GetWorkUserInfoVo;
import com.huawei.housekeeper.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户信息查询
 *
 * @author y00464350
 * @since 2022-02-14
 */
@RestController
@Validated
@RequestMapping("/user")
@Api(tags = "用户控制层")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/info")
    @ApiOperation(value = "创建用户信息")
    public Result<String> createUserInfo(@Valid @RequestBody CreateUserDto createUserInfoDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.createUserInfo(createUserInfoDto));
    }

    @PostMapping(value = "/info/batch")
    @ApiOperation(value = "分页查询用户信息")
    public Result<ListRes<GetUserVo>> pageQueryUsers(@Valid @RequestBody PageQueryUserDto pageQueryUserDto,
        @RequestHeader(defaultValue = HttpHeaders.AUTHORIZATION, name = "Authorization") String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.pageQueryUsers(pageQueryUserDto));
    }

    @PostMapping(value = "/info/condition")
    @ApiOperation(value = "查询用户信息")
    public Result<ListRes<GetUserVo>> getUserInfo(@Valid @RequestBody GetUserDto userDto,
        @RequestHeader(defaultValue = HttpHeaders.AUTHORIZATION, name = "Authorization") String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.getUserInfo(userDto));
    }

    @PostMapping(value = "/info/userinfo")
    @ApiOperation(value = "用户信息详情")
    public Result<GetUserInfoVo> getUserInfoById(
        @RequestHeader(defaultValue = HttpHeaders.AUTHORIZATION, name = "Authorization") String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.getUserInfoById());
    }

    @PostMapping(value = "/tenant/userInfo")
    @ApiOperation(value = "租户查用户信息列表")
    public Result<ListRes<GetTenantUserInfoVo>> getUserInfoList(@Valid @RequestBody PageQueryUserDto pageQueryUserDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain,
        @RequestHeader(defaultValue = HttpHeaders.AUTHORIZATION, name = "Authorization") String authorization) {
        return Result.createResult(userService.getUserInfoList(pageQueryUserDto));
    }

    @PostMapping(value = "/info/workInfo")
    @ApiOperation(value = "查询工人信息")
    public Result<GetWorkUserInfoVo> getWorkInfo(@Valid @RequestBody GetWorkUserInfoDto workUserInfoDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.getWorkInfo(workUserInfoDto));
    }

    @DeleteMapping(value = "/info/{userId}")
    @ApiOperation(value = "删除用户信息")
    public Result<Boolean> deleteUserInfoById(
        @Length(min = 3, max = 32, message = "固定长度：32") @PathVariable("userId") String userId,
        @RequestHeader(defaultValue = HttpHeaders.AUTHORIZATION, name = "Authorization") String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(userService.deleteUserInfoByUserId(userId));
    }
}
