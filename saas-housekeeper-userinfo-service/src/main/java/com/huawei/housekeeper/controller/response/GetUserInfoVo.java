/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户响应vo
 *
 * @author wwx1136431
 * @since 2022/3/21 15:18
 */
@Getter
@Setter
@ApiModel("查询用户详情vo")
public class GetUserInfoVo {
    @ApiModelProperty("手机号")
    private String phoneNo;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("地址")
    private String defaultAddress;
}
