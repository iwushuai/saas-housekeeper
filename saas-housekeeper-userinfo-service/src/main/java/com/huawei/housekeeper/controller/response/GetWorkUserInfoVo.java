/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户响应vo
 *
 * @author wwx1136431
 * @since 2022/3/2 15:18
 */
@Getter
@Setter
@ApiModel("查询工人信息vo")
public class GetWorkUserInfoVo {
    @TableField("user_id")
    private String userId;

    @TableField("USER_NAME")
    private String userName;

    @TableField("PHONE_NO")
    private String phoneNo;
}
