package com.huawei.housekeeper.common.enums;

import java.util.Optional;

import lombok.Getter;

@Getter
public enum AccountTypeEnum {
    ACCOUNT_USER(1, "user"),

    ACCOUNT_WORKER(2, "worker"),

    ACCOUNT_TENANT(3, "tenant");

    private int code;

    private String message;

    AccountTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Optional<AccountTypeEnum> ofAccountType(Integer type) {
        if (type == null) {
            return Optional.ofNullable(null);
        }
        for (AccountTypeEnum value : AccountTypeEnum.values()) {
            if (value.getCode() == type) {
                return Optional.ofNullable(value);
            }
        }
        return Optional.ofNullable(null);
    }
}
