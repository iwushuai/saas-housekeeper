/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.common.enums;

import com.huawei.housekeeper.commonutils.constants.BaseCode;

import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {
    FORBIDDEN(550001, "没有权限"),

    BUSINESS(550002, "业务异常"),

    UNKNOWN(550003, "未知异常"),

    PARAM_INVALID(550004, "参数非法"),

    USERNAME_OR_PASSWORD_ERROR(550005, "用户名或者密码错误"),

    TOKEN_EXPIRED(550006, "token过期"),

    TOKEN_ERROR(550007, "token错误"),

    IS_EMPTY(550008, "参数为空"),

    USERNAME_REPEATED(550009, "用户名重复!"),

    DELETE_ERROR(550010, "不能删除当前用户"),

    USER_EMPTY(550011, "用户不存在");

    private int code;

    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}