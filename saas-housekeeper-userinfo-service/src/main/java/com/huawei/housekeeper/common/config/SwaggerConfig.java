/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.config;

import com.huawei.housekeeper.commonutils.constants.BaseConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger2配置类
 *
 * @since 2022-01-05
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${swagger.profiles}")
    private String swaggerProfiles;

    @Value("${swagger.basePackage}")
    private String basePackage;

    /**
     * 配置Swagger的bean实例
     *
     * @return
     */
    @Bean
    public Docket docket(Environment environment) {
        // 获取要显示Swagger的环境
        String[] profileStr = swaggerProfiles.split(BaseConstant.Symbol.COMMA);
        Profiles profiles = Profiles.of(profileStr);

        // 是否启用swagger，如果为false则swagger不能再浏览器中访问
        boolean isShow = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("用户模块API")
                .enable(isShow)
                // 通过select()方法配置扫描接口
                .select()
                // 指定扫描的api包
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("用户模块API")
                .description("家政服务saas的userinfo模块")
                .version("1.0")
                .build();
    }
}