
package com.huawei.housekeeper.common.filters;

import com.huawei.housekeeper.common.enums.ErrorCode;
import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.commonutils.utils.JsonUtil;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 当访问接口没有权限时，自定义的返回结果
 */
@Component
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e)
        throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JsonUtil.objectToJson(Result.createResult(ErrorCode.FORBIDDEN)));
        response.setStatus(403);
        // Aspect无法处理Adapter中错误 Adapter先执行
        response.getWriter().flush();
    }
}
