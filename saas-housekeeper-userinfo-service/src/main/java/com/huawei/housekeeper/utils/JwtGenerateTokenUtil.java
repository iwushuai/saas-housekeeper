/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.utils;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.huawei.saashousekeeper.context.TenantContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.huawei.housekeeper.commonutils.constants.CommonConstants;
import com.huawei.housekeeper.dao.entity.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 生成jwt
 *
 * @author y00464350
 * @since 2022-02-11
 */
@Component
public class JwtGenerateTokenUtil {

    private static final String CLAIM_KEY_CREATED = "created";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    /**
     * 根据用户信息生成token
     *
     * @param user 用户信息
     */
    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(Claims.SUBJECT, user.getUserId());
        claims.put(CommonConstants.User.USER_NAME, user.getUserName());
        claims.put(CommonConstants.User.ROLE, user.getUserRole());
        claims.put(CLAIM_KEY_CREATED, new Date(System.currentTimeMillis()));
        claims.put(CommonConstants.User.USER_DOMAIN, TenantContext.getDomain());
        SecretKey secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), SignatureAlgorithm.HS512.getJcaName());
        return tokenHead + Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                .signWith(secretKey)
                .compact();
    }
}