/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 任务大厅消息
 *
 * @author lWX1128557
 * @since 2022-02-28
 */
@Getter
@Setter
public class TaskMessageEntity {

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 抢单动作
     */
    private String action;

    /**
     * 工人id
     */
    private String employeeId;

    /**
     * 接单/完成/取消时间
     */
    private Date time;
}