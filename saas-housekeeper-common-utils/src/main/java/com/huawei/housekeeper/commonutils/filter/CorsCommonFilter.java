
package com.huawei.housekeeper.commonutils.filter;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * cors跨域过滤器
 *
 * @author lWX1128557
 * @since 2022-04-25
 */
public class CorsCommonFilter implements WebMvcConfigurer {

    private static final Integer MAX_AGE = 3600;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            // 当allowCredentials为真时，allowedorigin不能包含特殊值"*"，因为不能在"访问-控制-起源“响应头中设置该值。
            // 要允许凭证到一组起源，显示地列出它们，或者考虑使用"allowedOriginPatterns”代替
            .allowCredentials(true)
            .allowedMethods("*")
            .allowedHeaders("*")
            .allowedOriginPatterns("*") // 是否允许所有的域请求
            .maxAge(3600);
    }
}
