/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.constants;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import lombok.Getter;

import java.util.Optional;

/**
 * 功能描述 抢单或完成任务
 *
 * @since 2022-03-08
 */
@Getter
public enum TaskOrderAction {
    TASK_ORDER_WAITING("waiting", "任务待领取"),
    TASK_ORDER_ACCEPT("accept", "任务已接收"),
    TASK_ORDER_CANCEL("cancel", "任务已取消"),
    TASK_ORDER_FINISHED("finished", "任务已完成");

    private String action;

    private String message;

    TaskOrderAction(String action, String message) {
        this.action = action;
        this.message = message;
    }

    /**
     * 根据action获取枚举类
     *
     * @param action
     * @return {@link TaskOrderAction}
     */
    public static Optional<TaskOrderAction> ofAction(String action) {
        if (StringUtils.isEmpty(action)) {
            return Optional.ofNullable(null);
        }
        for (TaskOrderAction value : TaskOrderAction.values()) {
            if (value.getAction().equals(action)) {
                return Optional.ofNullable(value);
            }
        }
        return Optional.ofNullable(null);
    }
}