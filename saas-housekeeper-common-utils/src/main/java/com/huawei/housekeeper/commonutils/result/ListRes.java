/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 返回List或者分页查询响应对象
 *
 * @Author l84165417
 * @Date 2022/1/27 9:09
 */
@Data
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "返回List或者分页查询响应对象")
public class ListRes<T> {
    @ApiModelProperty(value = "返回查询结果")
    private List<T> records;

    @ApiModelProperty(value = "总条数")
    private Integer total;
}
