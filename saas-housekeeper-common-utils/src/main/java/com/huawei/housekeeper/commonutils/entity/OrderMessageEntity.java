/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.entity;

import lombok.Getter;
import lombok.Setter;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Setter
@Getter
/**
 * 订单中心消息
 *
 * @author lWX1128557
 * @since 2022-02-28
 */
public class OrderMessageEntity {

    /**
     * 顾客id
     */
    private String customerId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 预约时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date appointment;

    /**
     * 服务地址
     */
    private String address;

    /**
     * 顾客姓名
     */
    private String customerName;

    /**
     * 顾客电话
     */
    private String customerPhone;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 服务详情
     */
    private ServiceDetail serviceDetail;

    /**
     * 订单数量
     */
    private Integer amount;

    /**
     * 支付价格
     */
    private BigDecimal payment;

    /**
     * 订单动作
     */
    private String taskOrderAction;

    @Setter
    @Getter
    public static class ServiceDetail {

        private List<Selections> selections;
    }

    @Setter
    @Getter
    public static class Selections {

        private String specName;

        private String selection;
    }
}