/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */
package com.huawei.housekeeper.commonutils.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.huawei.housekeeper.commonutils.constants.BaseCode;
import com.huawei.housekeeper.commonutils.enums.BaseCodeMsg;
import com.huawei.housekeeper.commonutils.exception.BusinessException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.function.Supplier;

/**
 * 请求返回工具类
 *
 * @author y00464350
 * @since 2022-02-23
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
@ApiModel(value = "响应对象")
public class Result<T> {
    @ApiModelProperty("状态码")
    private Integer code;

    @ApiModelProperty("描述信息")
    private String message;

    @ApiModelProperty("实体数据")
    private T result;

    public Result() {
    }

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.result = data;
    }

    public Result(Integer code, String message) {
        this(code, message, null);
    }

    /**
     * 创建数据返回体
     *
     * @param code    状态码
     * @param message 描述信息
     * @return Result 返回体
     */
    public static <T> Result<T> createResult(int code, String message) {
        return new Result<T>(code, message);
    }

    /**
     * 创建数据返回体
     *
     * @param code    状态码
     * @param message 描述信息
     * @param data    实体数据
     * @return Result 返回体
     */
    public static <T> Result<T> createResult(int code, String message, T data) {
        return new Result<T>(code, message, data);
    }

    /**
     * 创建数据返回体
     *
     * @param codeMsg 状态枚举类
     * @return Result 返回体
     */
    public static <T> Result<T> createResult(BaseCode codeMsg) {
        return new Result<T>(codeMsg.getCode(), codeMsg.getMessage());
    }

    /**
     * 创建数据返回体
     *
     * @param codeMsg 状态枚举类
     * @param data    实体数据
     * @return Result 返回体
     */
    public static <T> Result<T> createResult(BaseCode codeMsg, T data) {
        return new Result<T>(codeMsg.getCode(), codeMsg.getMessage(), data);
    }

    /**
     * 创建数据返回体
     *
     * @param data 实体数据
     * @return
     */
    public static <T> Result<T> createResult(T data) {
        return new Result<T>(BaseCodeMsg.SUCCESS.getCode(), BaseCodeMsg.SUCCESS.getMessage(), data);
    }

    /**
     * feign调用使用
     *
     * @param function 函数
     * @return Result 结果
     */
    public static <T> Result<T> feignResult(Supplier<Result<T>> function) {
        Result<T> result = function.get();
        if (result.getCode() != BaseCodeMsg.SUCCESS.getCode()) {
            throw new BusinessException(result.getCode(), result.getMessage());
        }
        return result;
    }
}