/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 分页查询时，入参继承这个类
 *
 * @author l84165417
 * @since  2022/1/27 9:09
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "分页查询请求对象的父类")
public class PageRequest {

    @NotNull(message = "必填")
    @Min(value = 0, message = "最小值：0")
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;

    @NotNull(message = "必填")
    @Min(value = 1, message = "最小值：1")
    @ApiModelProperty(value = "每页查询大小", required = true)
    private Integer size;

}
