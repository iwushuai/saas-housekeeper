/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2019. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.exception;

import com.huawei.housekeeper.commonutils.enums.ErrorCode;

/**
 * 参数错误异常
 *
 * @author q00197955
 * @version [版本号, 2019-07-23]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ParameterNotValidException extends ErrorCodeException {
    /**
     * 构造函数
     */
    public ParameterNotValidException() {
        super(ErrorCode.PARAM_INVALID.getCode());
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     */
    public ParameterNotValidException(int errorCode) {
        super(errorCode);
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 错误描述
     */
    public ParameterNotValidException(int errorCode, String message) {
        super(errorCode, message);
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param t 原始异常
     */
    public ParameterNotValidException(int errorCode, String message, Throwable t) {
        super(errorCode, message, t);
    }

    /**
     * 构造函数
     * @param message 错误描述
     */
    public ParameterNotValidException(String message) {
        super(ErrorCode.PARAM_INVALID.getCode(), message);
    }

    /**
     * 构造函数
     * @param message 错误描述
     * @param t 原始异常
     */
    public ParameterNotValidException(String message, Throwable t) {
        super(ErrorCode.PARAM_INVALID.getCode(), message, t);
    }
}
