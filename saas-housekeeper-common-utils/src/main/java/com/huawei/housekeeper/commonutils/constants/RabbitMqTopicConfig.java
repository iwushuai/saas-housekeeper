/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.constants;

/**
 * 功能描述 amq队列主题配置
 *
 * @since 2022-03-03
 */
public interface RabbitMqTopicConfig {
    /**
     * 租户标识字段
     */
    String RABBITMQ_HEADER_TENANT = "tenantDomain";

    /**
     * 任务大厅队列
     */
    String QUEUE_ORDER = "task.order";

    /**
     * 订单中心队列
     */
    String QUEUE_TASK = "order.task";

    /**
     * 交换机
     */
    String EXCHANGE_TOPIC_AMQ = "amq.topic";

    /**
     * 订单服务发送订单消息到任务大厅
     */
    String ROUTINGKEY_TASK_ORDER_NEWORDER = "task.order.newOrder";

    /**
     * 发送消息到消息中心
     */
    String MESSAGE_TASK_ORDER = "message.task.order";

    /**
     * 发送消息到订单中心
     */
    String ORDER_TASK_DO_TASK = "order.task.doTask";

}