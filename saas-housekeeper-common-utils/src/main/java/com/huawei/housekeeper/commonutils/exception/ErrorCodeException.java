/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 带错误码的异常
 *
 * @author q00197955
 * @version [版本号, 2019-07-22]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Getter
@Setter
public class ErrorCodeException extends RuntimeException {
    /**
     * 错误码
     */
    private int errorCode;

    /**
     * 负载对象
     */
    private Object payload;

    /**
     * 构造函数
     * @param errorCode 错误码
     */
    public ErrorCodeException(int errorCode) {
        super();
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 错误描述
     */
    public ErrorCodeException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 错误描述
     * @param t 原始异常
     */
    public ErrorCodeException(int errorCode, String message, Throwable t) {
        super(message, t);
        this.errorCode = errorCode;
    }
}
