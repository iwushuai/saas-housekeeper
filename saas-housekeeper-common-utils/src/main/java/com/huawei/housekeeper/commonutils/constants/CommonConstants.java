package com.huawei.housekeeper.commonutils.constants;

/**
 * 功能描述
 *
 * @since 2022-02-08
 */
public interface CommonConstants {

    interface User {
        // 用户ID
        static final String USER_ID = "user_id";

        // 用户密码
        static final String USER_PAW = "user_paw";

        // 用户名称
        static final String USER_NAME = "user_name";

        // 用户schema
        static final String USER_DOMAIN = "user_domain";

        // 用户角色
        static final String ROLE = "role";
    }

}
