/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.commonutils.entity;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 消息队列实体类
 *
 * @since 2022-02-16
 */
@Getter
@Setter
public class MessageServiceEntity {
    /**
     *  playload 标题
     */
    private String title;

    /**
     * msg 具体消息
     */
    private String msg;

    /**
     * 链接
     */
    private String link;

    /**
     * 用户Id
     */
    private String userId;

    /**
     * 发送时间
     */
    private Date time;
}
