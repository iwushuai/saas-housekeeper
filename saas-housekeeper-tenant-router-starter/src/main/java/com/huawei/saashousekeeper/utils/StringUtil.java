/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.utils;

import com.huawei.saashousekeeper.constants.Constants;

import org.apache.commons.lang.StringUtils;

/**
 * string工具类
 *
 * @since 2022-07-26
 */
public class StringUtil {
    /**
     * 截取
     *
     * @param url url
     * @return 结果
     */
    public static String getUri(String url) {
        return StringUtils.isBlank(url) ? url : url.split(Constants.QUEST_MARK)[0];
    }
}
