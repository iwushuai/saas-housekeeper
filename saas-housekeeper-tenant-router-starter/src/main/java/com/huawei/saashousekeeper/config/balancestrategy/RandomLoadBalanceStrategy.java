/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.balancestrategy;

import com.huawei.saashousekeeper.config.dynamicdatasource.SnapshotDataSource;
import com.huawei.saashousekeeper.constants.LoadBalanceStrategyEnum;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机算法
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public class RandomLoadBalanceStrategy implements LoadBalanceStrategy {
    @Override
    public String getStrategyType() {
        return LoadBalanceStrategyEnum.RANDOM.getCode();
    }

    @Override
    public SnapshotDataSource get(List<SnapshotDataSource> dataSourceList) {
        if (dataSourceList.size() == 1) {
            return dataSourceList.get(0);
        }
        return dataSourceList.get(ThreadLocalRandom.current().nextInt(dataSourceList.size()));
    }
}
