/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.customedprocessor;

import com.huawei.saashousekeeper.dbpool.PoolStrategy;

/**
 * 连接池刷新判断处理器
 *
 * @author lWX1156935
 * @since 2022-06-29
 */
public interface PoolRefreshProcessor extends PoolStrategy {
    /**
     * 检查
     *
     * @param origin 原始属性
     * @param current 最新属性
     * @return 检查结果
     */
    boolean check(Object origin, Object current);
}
