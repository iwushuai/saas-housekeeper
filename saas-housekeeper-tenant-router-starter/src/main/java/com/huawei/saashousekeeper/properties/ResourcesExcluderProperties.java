/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.properties;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 过滤资源
 *
 * @author lWX1128557
 * @since 2022-04-19
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "spring.schema.filters")
@RefreshScope
public class ResourcesExcluderProperties {

    private String[] excludedUris;
}
