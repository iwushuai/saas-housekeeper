/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.properties;

import com.huawei.saashousekeeper.context.TenantContext;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.Map;

/**
 * 租户schema存储
 *
 * @since 2022-02-14
 */
@ConfigurationProperties(prefix = "spring.schema")
@RefreshScope
@Log4j2
public class TenantProperties {
    private Map<String, String> tenantMap;

    private boolean databaseRouteEnabled;

    private boolean rabbitmqRouteEnabled;

    public boolean isDatabaseRouteEnabled() {
        return databaseRouteEnabled;
    }

    public void setDatabaseRouteEnabled(boolean databaseRouteEnabled) {
        this.databaseRouteEnabled = databaseRouteEnabled;
    }

    public boolean isRabbitmqRouteEnabled() {
        return rabbitmqRouteEnabled;
    }

    public void setRabbitmqRouteEnabled(boolean rabbitmqRouteEnabled) {
        this.rabbitmqRouteEnabled = rabbitmqRouteEnabled;
    }

    public Map<String, String> getTenantMap() {
        return tenantMap;
    }

    public void setTenantMap(Map<String, String> tenantMap) {
        this.tenantMap = tenantMap;
    }

    /**
     * 获取schema
     *
     * @return schema
     */
    public String getSchema() {
        log.info("tenantDomain: " + TenantContext.getDomain());
        String schema = tenantMap == null ? null : tenantMap.get(TenantContext.getDomain());
        log.info("schema: " + schema);
        return schema;
    }
}
