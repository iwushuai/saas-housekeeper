/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool.druid;

/**
 * Druid 配置属性
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface DruidConstants {
    String FIELD_NAME = "druid.name";

    String FIELD_MIN_IDLE = "druid.minIdle";

    String FIELD_MAX_WAIT = "druid.maxWait";

    String FIELD_INITIAL_SIZE = "druid.initialSize";

    String FIELD_MAX_ACTIVE = "druid.maxActive";

    String FIELD_TEST_WHILE_IDLE = "druid.testWhileIdle";

    String FIELD_TEST_ON_BORROW = "druid.testOnBorrow";

    String FIELD_VALIDATION_QUERY = "druid.validationQuery";

    String FIELD_USE_GLOBAL_DATA_SOURCE_STAT = "druid.useGlobalDataSourceStat";

    String FIELD_ASYNC_INIT = "druid.asyncInit";

    String FIELD_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "druid.timeBetweenEvictionRunsMillis";

    String FIELD_TIME_BETWEEN_LOG_STATS_MILLIS = "druid.timeBetweenLogStatsMillis";

    String FIELD_MIN_EVICTABLE_IDLE_TIME_MILLIS = "druid.minEvictableIdleTimeMillis";

    String FIELD_MAX_EVICTABLE_IDLE_TIME_MILLIS = "druid.maxEvictableIdleTimeMillis";

    String FIELD_FAIL_FAST = "druid.failFast";

    String FIELD_PHY_TIMEOUT_MILLIS = "druid.phyTimeoutMillis";

    String FIELD_KEEP_ALIVE = "druid.keepAlive";

    String FIELD_POOL_PREPARED_STATEMENTS = "druid.poolPreparedStatements";

    String FIELD_INIT_VARIANTS = "druid.initVariants";

    String FIELD_INIT_GLOBAL_VARIANTS = "druid.initGlobalVariants";

    String FIELD_USE_UNFAIR_LOCK = "druid.useUnfairLock";

    String FIELD_KILL_WHEN_SOCKET_READ_TIMEOUT = "druid.killWhenSocketReadTimeout";

    String FIELD_MAX_POOL_PREPARED_STATEMENT_PER_CONNECTION_SIZE = "druid.maxPoolPreparedStatementPerConnectionSize";

    String FIELD_INIT_CONNECTION_SQLS = "druid.initConnectionSqls";

    String FIELD_FILTERS = "druid.filters";

    String FIELD_CLEAR_FILTERS_ENABLE = "druid.clearFiltersEnable";

    String FIELD_RESET_STAT_ENABLE = "druid.resetStatEnable";

    String FIELD_NOT_FULL_TIMEOUT_RETRY_COUNT = "druid.notFullTimeoutRetryCount";

    String FIELD_MAX_WAIT_THREAD_COUNT = "druid.maxWaitThreadCount";

    String CONFIG_STR = "config";

    String STAT_STR = "stat";
}
