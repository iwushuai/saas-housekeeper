import { AxiosPromise } from 'axios';
import { axios } from './api.request';

interface Response {
    code: number;
    result: any;
    message: string;
}

export function queryFn(url: string, params: any): Promise<Response> {
    return axios.request({
        url,
        method: params.method,
        data: params.data,
    });
}

/**
 * 登录接口
 * @param params
 */
export function userLogin(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/login/user-login', params);
}

/**
 * 获取所有消息 GET
 * @param params
 */
export function getAllMsg(params: any) {
    return queryFn('/saas-message/msg/all-msg', params);
}

/**
 * 获取已读/未读消息 GET
 * @param params
 */
export function getMsg(params: any) {
    return queryFn(`/saas-message/msg/msg/${params.data.status}`, params);
}

/**
 * 获取未读消息数量 GET
 * @param params
 */
export function getCount(params: any) {
    return queryFn('/saas-message/msg/count', params);
}

/**
 * 将消息标为已读 POST
 * @param params
 */
export function setMsg(params: any) {
    return queryFn('/saas-message/msg/state', params);
}

/**
 * 删除已读消息 POST
 * @param params
 */
export function deleteMsg(params: any) {
    return queryFn('/saas-message/msg/del', params);
}

/**
 * 查询可购买服务列表
 * @param { pageIndex，pageSize，serviceName } params
 */
export function getServiceIndexList(params: { method: string; data: {} }) {
    return queryFn('/saas-public/housekeeper/getServiceIndexList', params);
}

/**
 * 查询我的订单列表
 * @param { pageIndex，pageSize，status } params
 */
export function getOrdersList(params: { method: string; data: {} }) {
    return queryFn('/saas-order/order/customer-orders', params);
}

/**
 * 查询订单详情接口
 * @param params
 * @orderId 订单id
 */
export function getOrderDetails(params: {
    method: string;
    data: {
        orderId: string;
    };
}) {
    return queryFn(`/saas-order/order/order/orderId/${params.data.orderId}`, params);
}

/**
 * 查看个人已接任务
 * @param { pageIndex，pageSize，status } params
 */
export function getpageQueryTaskList(params: { method: string; data: {} }) {
    return queryFn('/saas-task-service/housekepper/task/myTasks', params);
}

/**
 * 雇员任务查询接口
 * @param { pageIndex，pageSize，status } params
 */
export function getpageQueryTasks(params: { method: string; data: {} }) {
    return queryFn('/saas-task-service/housekepper/task/tasks', params);
}

/**
 * 抢单或完成任务
 * @param { pageIndex，pageSize，status } params
 */
export function getpagedoTask(params: { method: string; data: {} }) {
    return queryFn('/saas-task-service/housekepper/task/doTask', params);
}

/**
 * 查询个人已接任务详情，显示用户名称和电话
 * @param { pageIndex，pageSize，status } params
 */
export function getTaskOrderDetails(params: { method: string; data: {} }) {
    return queryFn('/saas-task-service/housekepper/task/myTask', params);
}

/**
 * 用户注册 POST
 * @param params
 */
export function registerUser(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/user/info', params);
}

/**
 * 查询工人信息
 * @param { pageIndex，pageSize，status } params
 */
export function getTaskworkInfo(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/user/info/workInfo', params);
}
