import axios, { AxiosInstance } from 'axios';
import { ErrorCodeMap } from '@/utils/errorCodeMap';
import { ElMessage } from 'element-plus';
import { getCookie } from '@/utils/utils';

class HttpRequest {
    baseUrl: any;

    queue: any;

    constructor(baseUrl: any) {
        this.baseUrl = baseUrl;
        this.queue = {};
    }

    getConfig() {
        return {
            baseURL: this.baseUrl,
            headers: {},
        };
    }

    destroy(url: any) {
        delete this.queue[url];
    }

    interceptors(instance: AxiosInstance, url: string | number) {
        // 请求拦截
        instance.interceptors.request.use(
            (config) => {
                if (!['/saas-user-info/login/user-login', '/saas-user-info/user/info'].includes(config.url as string)) {
                    const token = getCookie('SaaS_Token');
                    if (!token) {
                        window.location.href = '#/login';
                    }
                    config.headers.Authorization = token;
                }
                if (!Object.keys(this.queue).length) {
                }
                this.queue[url] = true;
                return config;
            },
            (error) => Promise.reject(error),
        );
        // 响应拦截
        instance.interceptors.response.use(
            (res): any => {
                const { code } = res.data;
                if (code === 110006 || code === 110007 || code === 100001) {
                    ElMessage.error(`${res.data.message}请重新登录`);
                    window.location.href = '#/login';
                    return
                }
                // 错误响应统一此处处理
                if (code !== 200) {
                    if (Object.prototype.hasOwnProperty.call(ErrorCodeMap, code)) {
                        ElMessage.error(ErrorCodeMap[code]);
                        return Promise.reject(res.data);
                    }
                    ElMessage.error(res.data.message || '未知错误');
                    return Promise.reject(res.data);
                }
                this.destroy(url);
                return res.data;
            },
            (error: { response: any }) => {
                this.destroy(url);
                return Promise.reject(error);
            },
        );
    }

    request(opt: any): Promise<any> {
        const instance = axios.create();
        opt = Object.assign(this.getConfig(), opt);
        this.interceptors(instance, opt.url);
        return instance(opt);
    }
}

export default HttpRequest;
