/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.gateway.enums;


import com.huawei.housekeeper.commonutils.constants.BaseCode;
import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {
    UNKNOWN(100001, "未知异常"),

    TOKEN_EXPIRED(100002, "token失效"),

    TOKEN_ILLEGAL(100003, "token非法"),

    SERVER_ERROR(100004, "系统异常"),

    INVALID(100005, "参数非法"),

    PASSWORD(100006, "密码错误"),

    FORBIDDEN(100007, "没有权限"),

    BUSINESS(100008, "业务异常"),

    PARTNER_CENTER_SERVER_ERROR(100009, "系统异常"),

    TOKEN_UPDATE(100010, "token更新失败"),

    JSON_ERROR(100011, "json异常"),

    W3_TOKEN(100012, "获取W3token失败");

    private int code;

    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}