
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.gateway;

import com.huawei.housekeeper.commonutils.utils.MessageServiceUtil;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * 主启动类
 *
 * @author y00464350
 * @since 2022-02-15
 */
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = "com.huawei.**",
    excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {MessageServiceUtil.class})})
@Log4j2
public class SaasHousekeeperGatewayApplication {

    public static void main(String[] args) {
        log.info("saas housekeeper gateway start...");
        SpringApplication.run(SaasHousekeeperGatewayApplication.class, args);
        log.info("saas housekeeper gateway end...");
    }
}
