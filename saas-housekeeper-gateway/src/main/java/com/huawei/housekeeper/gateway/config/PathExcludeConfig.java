package com.huawei.housekeeper.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述
 *
 * @author wWX1136431
 * @since 2022-03-22
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "gateway")
public class PathExcludeConfig {
    private String[] excludedAuthpages;
}
