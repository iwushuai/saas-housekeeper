/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.saashousekeepereventtracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * 主启动类
 *
 * @author lWX1128557
 * @since 2022-07-20
 */
@ComponentScan(basePackages = {"com.huawei.**"},
    excludeFilters = {@ComponentScan.Filter(type = FilterType.REGEX,
        pattern = {"com\\.huawei\\.housekeeper\\.commonutils\\.utils\\.MessageServiceUtil"})})
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class SaasHousekeeperEventTrackingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperEventTrackingApplication.class, args);
    }

}
