/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.saashousekeepereventtracking.common.constant;

/**
 * 日志信息常量
 *
 * @author lWX1128557
 * @since 2022-07-01
 */
public interface Constant {

    /**
     * 信息
     */
    String INFORMATION = "information";

    /**
     * 动作
     */
    String ACTIONS = "actions";

    /**
     * 页码
     */
    String PAGE = "page";

    /**
     * 时间戳
     */
    String TS = "ts";

    /**
     * 模块
     */
    String MODEL = "model";

    /**
     * 服务id
     */
    String SERVICE_ID = "serviceId";

    /**
     * token
     */
    String TOKEN = "token";
}
