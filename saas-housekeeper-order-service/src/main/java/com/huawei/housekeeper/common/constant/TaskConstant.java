/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.constant;

/**
 * 任务大厅服务抢单动作
 *
 * @author lWX1128557
 * @since 2022-03-07
 */
public interface TaskConstant {

    /**
     * 接单
     */
    String ACCEPT = "accept";

    /**
     * 订单完成
     */
    String FINISHED = "finished";

    /**
     * 订单取消
     */
    String CANCEL = "cancel";
}
