/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 服务描述的Json封装
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceDetail {
    /**
     * 服务信息
     */
    @JsonProperty(value = "result")
    @JSONField(name = "serviceDesc")
    private ServiceDesc serviceDesc;

    /**
     * 服务规格
     *
     * @author lWX1128557
     * @since 2022-02-23
     */
    @Data
    public static class Selections {
        /**
         * 规格选项id
         */
        private Integer optionId;

        /**
         * 中间表id
         */
        private Integer selectionId;

        /**
         * 规格选项名称
         */
        private String optionName;

        /**
         * 规格名称
         */
        private String specName;
    }

    /**
     * 服务详情
     *
     * @author lWX1128557
     * @since 2022-02-23
     */
    @Data
    public static class ServiceDesc {
        /**
         * 规格选项id
         */
        private Integer skuId;

        /**
         * 服务规格
         */
        private List<Selections> selections;

        /**
         * 价格
         */
        private BigDecimal price;

        /**
         * 服务id
         */
        private Integer servcieId;

        /**
         * 服务名
         */
        private String serviceName;

        /**
         * 服务描述
         */
        private String serviceDesc;

        /**
         * 服务图片
         */
        private String imgSrc;
    }
}
