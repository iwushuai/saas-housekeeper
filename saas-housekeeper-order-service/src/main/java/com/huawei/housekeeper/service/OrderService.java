
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.commonutils.result.ListRes;
import com.huawei.housekeeper.controller.request.CreateOrderDto;
import com.huawei.housekeeper.controller.request.PageQueryOrderDto;
import com.huawei.housekeeper.controller.request.UpdateOrderDto;
import com.huawei.housekeeper.controller.response.GetOrderDetailsVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfCustomerVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfTenantVo;
import com.huawei.housekeeper.dao.entities.Order;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 订单服务层接口
 *
 * @author lWX1128557
 * @since 2022-03-01
 */
public interface OrderService extends IService<Order> {
    /**
     * 用户查询所有订单
     *
     * @param pageQueryOrderDto 分页
     * @return GetOrdersOfCustomerVo 用户订单列表
     */
    ListRes<GetOrdersOfCustomerVo> getOrdersByCustomer(PageQueryOrderDto pageQueryOrderDto);

    /**
     * 租户查询所有订单
     *
     * @param pageQueryOrderDto 分页
     * @return GetOrdersOfTenantVo 租户订单列表
     */
    ListRes<GetOrdersOfTenantVo> getOrdersByTenant(PageQueryOrderDto pageQueryOrderDto);

    /**
     * 订单号查询订单详情
     *
     * @param orderNumber 订单号请求
     * @return GetOrderDetailsVo 订单详情
     */
    GetOrderDetailsVo getOrderByOrderNumber(String orderNumber);

    /**
     * 订单Id查询订单详情
     *
     * @param orderId 订单Id
     * @return GetOrderDetailsVo 订单详情
     */
    GetOrderDetailsVo getOrderByOrderId(Integer orderId);

    /**
     * 用户下单
     *
     * @param orderDto 下单Dto
     * @return 下单结果
     * @throws JsonProcessingException JSon格式异常
     */
    Long saveOrder(CreateOrderDto orderDto) throws JsonProcessingException;

    /**
     * 更新订单
     *
     * @param taskMessage 消息体
     * @throws JsonProcessingException JSon格式异常
     */
    void updateOrder(String taskMessage) throws JsonProcessingException;

    /**
     * 用户更新订单
     *
     * @param updateOrderDto 用户更新订单状态
     * @return 更新影响结果数
     * @throws JsonProcessingException JSon格式异常
     */
    Integer updateOrder(UpdateOrderDto updateOrderDto) throws JsonProcessingException;
}
