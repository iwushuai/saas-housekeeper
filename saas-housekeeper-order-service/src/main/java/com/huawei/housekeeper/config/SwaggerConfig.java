/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.config;

import com.huawei.housekeeper.commonutils.constants.BaseConstant;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

/**
 * swagger配置
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Configuration
@EnableSwagger2
@RefreshScope
public class SwaggerConfig {
    @Value("${swagger.profiles}")
    private String swaggerProfiles;

    @Value("${swagger.basePackage}")
    private String basePackage;

    /**
     * swagger文档
     *
     * @param environment 环境
     * @return docket
     */
    @Bean
    public Docket docket(Environment environment) {
        // 获取要显示的swagger环境
        String[] profileStr = swaggerProfiles.split(BaseConstant.Symbol.COMMA);
        Profiles profiles = Profiles.of(profileStr);

        // 是否展示当前环境swagger
        boolean isShow = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
            .groupName("订单中心API")
            .enable(isShow)
            .select() // 扫描接口生成api文档
            // 通过包扫描
            .apis(RequestHandlerSelectors.basePackage(basePackage))
            .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("订单中心API").description("家政服务saas的order-service模块").version("1.0").build();
    }
}
