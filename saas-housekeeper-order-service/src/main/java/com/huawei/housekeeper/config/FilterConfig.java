
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.config;

import com.huawei.housekeeper.config.filter.JwtAuthenticationTokenFilter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 过滤器
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Configuration
public class FilterConfig {
    /**
     * 添加过滤器
     *
     * @return registrationBean
     */
    @Bean
    public FilterRegistrationBean<JwtAuthenticationTokenFilter> filterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(jwtAuthenticationTokenFilter());
        registrationBean.addUrlPatterns("/order/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    /**
     * 注入过滤器
     *
     * @return JwtAuthenticationTokenFilter
     */
    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }
}
