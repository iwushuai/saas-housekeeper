/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.commonutils.result.ListRes;
import com.huawei.housekeeper.controller.request.CreateTenantDto;
import com.huawei.housekeeper.controller.request.PageQueryTenantDto;
import com.huawei.housekeeper.controller.request.UpdateTenantDto;
import com.huawei.housekeeper.controller.response.GetMigrationInformationVo;
import com.huawei.housekeeper.controller.response.GetTenantDetailVo;
import com.huawei.housekeeper.dao.entities.Tenant;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * service
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
public interface TenantService extends IService<Tenant> {
    /**
     * 租户注册
     *
     * @param createTenantDto 租户注册Dto
     * @return Integer 注册结果
     */
    String saveTenant(CreateTenantDto createTenantDto);

    /**
     * 租户查看注册状态
     *
     * @param tenantNumber 租户企业信用码
     * @return GetTenantDetailVo 租户详情Vo
     */
    GetTenantDetailVo getTenantDetail(String tenantNumber);

    /**
     * 超级管理员查看租户列表
     *
     * @param pageQueryTenantDto 租户分页Dto
     * @return List<GetTenantDetailVo/> 租户列表Vo
     */
    ListRes<GetTenantDetailVo> getTenantDetails(PageQueryTenantDto pageQueryTenantDto);

    /**
     * 修改租户状态
     *
     * @param updateTenantDto 租户状态
     * @return Integer 修改结果
     */
    Integer updateTenantStatus(UpdateTenantDto updateTenantDto);

    /**
     * 查询所有租户信息
     *
     * @return 所有租户信息
     */
    List<Tenant> getTenants();

    /**
     * 根据租户信用码查询租户状态
     *
     * @param number 租户信用码
     * @return 租户状态
     */
    Integer getStatusByTenantId(String number);

    /**
     * 刷新所有租户数据库
     *
     * @return 迁移信息
     */
    GetMigrationInformationVo migrationAllTenant();
}
