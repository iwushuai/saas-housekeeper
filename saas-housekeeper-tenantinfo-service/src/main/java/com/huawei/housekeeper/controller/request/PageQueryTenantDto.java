/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.huawei.housekeeper.commonutils.request.PageRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * 租户分页查询
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@Data
@ApiModel("租户分页查询对象")
public class PageQueryTenantDto extends PageRequest {
    @ApiModelProperty(value = "租户状态")
    @Min(value = 1, message = "范围：1,2,3")
    @Max(value = 3, message = "范围：1,2,3")
    private Integer status;

}
