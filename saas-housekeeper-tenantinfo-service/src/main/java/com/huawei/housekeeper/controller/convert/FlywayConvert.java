
package com.huawei.housekeeper.controller.convert;

import com.huawei.housekeeper.controller.response.GetMigrationInformationVo;
import com.huawei.housekeeper.dao.entities.MigrationInformation;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * flyway映射
 *
 * @author lWX1128557
 * @since 2022-04-27
 */
@Mapper(componentModel = "spring")
public interface FlywayConvert {

    /**
     * flyway映射实例
     */
    FlywayConvert INSTANCE = Mappers.getMapper(FlywayConvert.class);

    /**
     * flyway迁移信息
     *
     * @param migrationInformation 迁移信息
     * @return 迁移信息Vo
     */
    GetMigrationInformationVo toGetMigrationInformationVo(MigrationInformation migrationInformation);
}
