/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 创建租户Dto
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@ApiModel(value = "租户注册")
@Getter
@Setter
public class CreateTenantDto {
    @NotBlank
    @Length(max = 32, message = "长度范围：32")
    @ApiModelProperty(value = "租户名", required = true)
    private String name;

    @NotBlank
    @Length(max = 255, message = "长度范围：255")
    @ApiModelProperty(value = "租户企业信用码", required = true)
    private String number;

    @NotBlank
    @Length(min = 11, max = 11, message = "长度不对")
    @ApiModelProperty(value = "租户电话", required = true)
    private String phone;

    @NotBlank
    @Length(max = 32, message = "长度范围：32")
    @ApiModelProperty(value = "租户邮箱", required = true)
    private String email;

    @NotBlank
    @Length(max = 255, message = "长度范围：255")
    @ApiModelProperty(value = "具体内容")
    private String content;

    @NotBlank
    @Length(max = 32, message = "长度范围：32")
    @ApiModelProperty(value = "租户域名", required = true)
    private String domain;
}
