/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.controller.request.CreateTenantDto;
import com.huawei.housekeeper.controller.response.GetTenantDetailVo;
import com.huawei.housekeeper.service.TenantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

/**
 * 租户注册控制层
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@RestController
@RequestMapping(value = "/tenant")
@Api(value = "租户控制层")
@Valid
public class TenantController {
    @Autowired
    private TenantService tenantService;

    @PostMapping(value = "tenant")
    @ApiOperation(value = "租户注册")
    public Result<String> saveTenant(@RequestBody @Valid CreateTenantDto createTenantDto) {
        return Result.createResult(tenantService.saveTenant(createTenantDto));
    }

    @GetMapping(value = "tenant/{tenantNumber}")
    @ApiOperation(value = "根据企业信用代码查询租户详情")
    public Result<GetTenantDetailVo> getTenantDetail(
        @Pattern(regexp = "^\\d{18}$", message = "只能为18位数字") @PathVariable("tenantNumber") String tenantNumber) {
        return Result.createResult(tenantService.getTenantDetail(tenantNumber));
    }
}
