/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.model;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 超级管理员身份
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@Component
@Getter
@Setter
public class AdminContext {

    @Value("${adminContext.name}")
    private String name;

    @Value("${adminContext.password}")
    private String password;

    @Value("${adminContext.role}")
    private String role;
}
