
package com.huawei.housekeeper.common.model;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 租户路由配置表
 *
 * @author lWX1128557
 * @since 2022-04-08
 */
@ConfigurationProperties(prefix = "tenant.route")
@Component
@Getter
@Setter
public class TenantRouteProperties {
    /**
     * 租户标识：spring.schema.tenant-map.{tenantDomain}
     */
    private String key;

    /**
     * 读取的配置文件
     */
    private String application;

    /**
     * 读取的环境
     */
    private String profile;

    /**
     * 读取的分支
     */
    private String label;
}
