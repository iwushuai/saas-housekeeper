
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 主启动类
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@SpringBootApplication(scanBasePackages = "com.huawei.housekeeper")
@EnableEurekaClient
@EnableFeignClients
@Log4j2
public class SaasHousekeeperTenantinfoServiceApplication {

    /**
     * 租户启动入口
     *
     * @param args 形参
     */
    public static void main(String[] args) {
        log.info("tenant info module start...");
        SpringApplication.run(SaasHousekeeperTenantinfoServiceApplication.class, args);
        log.info("tenant info module start over...");
    }
}
