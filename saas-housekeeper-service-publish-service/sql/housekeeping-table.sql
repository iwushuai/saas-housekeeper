/*
 Navicat Premium Data Transfer

 Source Server         : huaweiyun-mysql
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 100.93.2.89:3306
 Source Schema         : housekeeping

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 19/01/2022 11:48:04
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_housekeeper_service
-- ----------------------------
DROP TABLE IF EXISTS `t_housekeeper_service`;
CREATE TABLE `t_housekeeper_service`
(
    `id`            int(0) NOT NULL AUTO_INCREMENT COMMENT '服务ID',
    `service_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务名称',
    `service_desc`  varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务描述',
    `created_by`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time`  datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`    varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time`  datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `servie_status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务状态',
    `delete_flag`   varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志',
    `revision`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    display_price   decimal(24, 6) comment '服务展示价格',
    img_src         varchar(500) comment '图片地址',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务主表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_service_option
-- ----------------------------
DROP TABLE IF EXISTS `t_service_option`;
CREATE TABLE `t_service_option`
(
    `id`               int(0) NOT NULL AUTO_INCREMENT COMMENT '选项ID',
    `specification_id` int(0) NOT NULL COMMENT '规格ID',
    `name`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项名称',
    `created_by`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time`     datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`       varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time`     datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `delete_flag`      varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志',
    `revision`         varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规格选项表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_service_selection
-- ----------------------------
DROP TABLE IF EXISTS `t_service_selection`;
CREATE TABLE `t_service_selection`
(
    `id`           int(0) NOT NULL AUTO_INCREMENT COMMENT '选集ID',
    `option_id`    int(0) NULL DEFAULT NULL COMMENT '选项ID',
    `sku_id`       int(0) NULL DEFAULT NULL COMMENT 'SKU_ID',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选集表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_service_sku
-- ----------------------------
DROP TABLE IF EXISTS `t_service_sku`;
CREATE TABLE `t_service_sku`
(
    `id`           int(0) NOT NULL AUTO_INCREMENT COMMENT '可选服务ID',
    `service_id`   int(0) NULL DEFAULT NULL COMMENT '服务ID',
    `price`        decimal(24, 6) NULL DEFAULT NULL COMMENT '价格',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可选服务表(SKU)' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_service_specification
-- ----------------------------
DROP TABLE IF EXISTS `t_service_specification`;
CREATE TABLE `t_service_specification`
(
    `id`           int(0) NOT NULL AUTO_INCREMENT COMMENT '规格ID',
    `service_id`   int(0) NULL DEFAULT NULL COMMENT '服务ID',
    `name`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格名称',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规格表' ROW_FORMAT = Compact;

drop table if exists t_customization;

/*==============================================================*/
/* Table: t_customization                                       */
/*==============================================================*/
create table t_customization
(
    config_id  int(32) not null
        auto_increment comment '配置ID',
    style_flag tinyint comment '主题标识',
    store_name varchar(50) comment '店名',
    tenant_id  varchar(50) comment '租户标识',
    primary key (config_id)
) engine = InnoDB
charset = UTF8;

alter table t_customization comment '租户配置表';

SET
FOREIGN_KEY_CHECKS = 1;