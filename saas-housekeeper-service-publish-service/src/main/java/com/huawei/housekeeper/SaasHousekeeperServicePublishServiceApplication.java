/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
public class SaasHousekeeperServicePublishServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperServicePublishServiceApplication.class, args);
    }

}