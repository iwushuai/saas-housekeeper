/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author jWX1116205
 * @since 2022-01-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询单个服务响应vo")
public class ServiceInfoRespVo {
    @ApiModelProperty("规格选项列表")
    private List<SpecsItem> specs;

    @ApiModelProperty("skus")
    private List<SkusItem> skus;

    private Long servcieId;

    private Date createTime;

    @ApiModelProperty(value = "服务名称", required = true)
    private String serviceName;

    @ApiModelProperty(value = "服务描述", required = true)
    private String serviceDesc;

    @ApiModelProperty("服务展示价格")
    private BigDecimal displayPrice;

    @ApiModelProperty("图片地址")
    private String imgSrc;

    @Data
    public static class SkusItem {
        @ApiModelProperty("SkuID")
        private Long skuId;

        @ApiModelProperty("可选服务")
        private List<OptionProperty> selections;

        @ApiModelProperty("价格")
        private BigDecimal price;
    }

    @Data
    public static class SpecsItem {
        @ApiModelProperty("规格Id")
        private Long specId;

        @ApiModelProperty("规格名称")
        private String name;

        @ApiModelProperty("规格选项")
        private List<OptionProperty> options;
    }

    @Data
    public static class OptionProperty {
        @ApiModelProperty("选项Id")
        private Long optionId;

        @ApiModelProperty("选项服务ID")
        private Long selectionId;

        @ApiModelProperty("选项名称")
        private String optionName;

        @ApiModelProperty("规格名称")
        private String specName;
    }
}