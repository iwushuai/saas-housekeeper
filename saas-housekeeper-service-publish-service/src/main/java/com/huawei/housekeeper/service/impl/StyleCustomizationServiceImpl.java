/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service.impl;

import com.huawei.housekeeper.commonutils.utils.CopyUtils;
import com.huawei.housekeeper.controller.request.CreateTenantStyleCustomizationDto;
import com.huawei.housekeeper.controller.request.UpdateTenantStyleCustomizationDto;
import com.huawei.housekeeper.controller.response.TenantStyleCustomizationVo;
import com.huawei.housekeeper.dao.entity.Customization;
import com.huawei.housekeeper.dao.mapper.CustomizationMapper;
import com.huawei.housekeeper.service.IStyleCustomizationService;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import com.huawei.saashousekeeper.context.TenantContext;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 功能描述 租户主题配置功能类
 *
 * @author jWX1116205
 * @since 2022-02-16
 */
@Slf4j
@Service
public class StyleCustomizationServiceImpl implements IStyleCustomizationService {
    @Autowired
    CustomizationMapper customizationMapper;

    /**
     * 创建服务配置或修改服务配置
     *
     * @param tenantStyleCustomizationDto 配置
     * @return {@link String}
     */
    @Override
    public String createTenantStyleCustomization(CreateTenantStyleCustomizationDto tenantStyleCustomizationDto) {
        LambdaQueryWrapper<Customization> selectCustomizationQuery = Wrappers.lambdaQuery();
        selectCustomizationQuery.eq(Customization::getTenantId, TenantContext.getDomain());
        Customization styleCustomization = customizationMapper.selectOne(selectCustomizationQuery);
        if (styleCustomization == null) {
            styleCustomization = new Customization();
            styleCustomization.setTenantId(TenantContext.getDomain());
            styleCustomization.setStyleFlag(Long.valueOf(tenantStyleCustomizationDto.getStyleFlag()));
            customizationMapper.insert(styleCustomization);
        } else {
            styleCustomization.setTenantId(TenantContext.getDomain());
            styleCustomization.setStyleFlag(Long.valueOf(tenantStyleCustomizationDto.getStyleFlag()));
            customizationMapper.updateById(styleCustomization);
        }
        return TenantContext.getDomain();
    }

    /**
     * 修改租户主题配置
     *
     * @param updateTenantStyleCustomizationDto 配置
     * @return {@link String}
     */
    @Override
    public String updateTenantStyleCustomization(UpdateTenantStyleCustomizationDto updateTenantStyleCustomizationDto) {
        Customization styleCustomization = new Customization();
        styleCustomization.setTenantId(TenantContext.getDomain());
        styleCustomization.setStyleFlag(Long.valueOf(updateTenantStyleCustomizationDto.getStyleFlag()));
        LambdaQueryWrapper<Customization> selectCustomizationQuery = Wrappers.lambdaQuery();
        selectCustomizationQuery.eq(Customization::getTenantId, TenantContext.getDomain());
        customizationMapper.update(styleCustomization, selectCustomizationQuery);
        return TenantContext.getDomain();
    }

    /**
     * 查询当前租户配置
     *
     * @return {@link TenantStyleCustomizationVo}
     */
    @Override
    public TenantStyleCustomizationVo getTenantStyleCustomizationVo() {
        LambdaQueryWrapper<Customization> selectCustomizationQuery = Wrappers.lambdaQuery();
        selectCustomizationQuery.eq(Customization::getTenantId, TenantContext.getDomain());
        Customization styleCustomization = customizationMapper.selectOne(selectCustomizationQuery);
        if (styleCustomization == null) {
            return null;
        }
        return CopyUtils.copyProperties(styleCustomization, new TenantStyleCustomizationVo());
    }
}