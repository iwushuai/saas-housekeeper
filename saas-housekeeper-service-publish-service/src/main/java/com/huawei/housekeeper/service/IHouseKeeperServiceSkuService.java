/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.controller.request.CreateServiceSelectionDto;
import com.huawei.housekeeper.controller.request.CreateServiceSpecificationDto;
import com.huawei.housekeeper.controller.request.DeleteServiceSkuDto;
import com.huawei.housekeeper.controller.request.DeleteSpecificationDto;
import com.huawei.housekeeper.controller.request.GetSkuDetailBySkuIdDto;
import com.huawei.housekeeper.controller.request.UpdateSeriviceSkuDto;
import com.huawei.housekeeper.controller.response.ServiceSkuInfoRespVo;

/**
 * 功能描述 服务SKU功能类
 *
 * @author jWX1116205
 * @since 2022-02-15
 */
public interface IHouseKeeperServiceSkuService {
    /**
     * 根据skuid拿到服务详细描述
     *
     * @param getSkuDetailBySkuIdDto
     * @return {@link ServiceSkuInfoRespVo}
     */
    ServiceSkuInfoRespVo getSkuDetailBySkuId(GetSkuDetailBySkuIdDto getSkuDetailBySkuIdDto);

    /**
     * 创建服务规格
     *
     * @param createServiceSpecificationDto
     * @return {@link Long}
     */
    Long createHousekeeperServiceSpecification(CreateServiceSpecificationDto createServiceSpecificationDto);

    /**
     * 创建服务选集
     *
     * @param createServiceSelectionDto
     * @return {@link String}
     */
    String createHousekeeperServiceSelection(CreateServiceSelectionDto createServiceSelectionDto);

    /**
     * 删除服务规格
     *
     * @param deleteServiceSpecificationDto
     * @return {@link Long}
     */
    Long deleteHousekeeperServiceSpecification(DeleteSpecificationDto deleteServiceSpecificationDto);

    /**
     * 删除服务Sku
     *
     * @param deleteServiceSkuDto
     * @return {@link Long}
     */
    Long deleteHousekeeperServiceSkuBySkuId(DeleteServiceSkuDto deleteServiceSkuDto);

    /**
     * 更新服务Sku
     *
     * @param updateSeriviceSkuDto
     * @return {@link Long}
     */
    Long updateSeriviceSku(UpdateSeriviceSkuDto updateSeriviceSkuDto);
}