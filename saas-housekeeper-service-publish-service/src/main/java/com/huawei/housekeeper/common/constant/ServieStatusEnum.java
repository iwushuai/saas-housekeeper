/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.constant;

import com.huawei.housekeeper.common.validator.EnumValidate;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述 服务状态枚举类
 *
 * @author jWX1116205
 * @since 2022-01-05
 */
public enum ServieStatusEnum implements EnumValidate<String> {

    SERVICE_APPLAY("1", "申请中"),
    SERVICE_RUNNING("2", "运行中"),
    SERVICE_STOP("3", "终止"),
    SERVICE_UPGRADE("4", "升级改造");

    private static final String FUN_DESC = "服务状态枚举";

    // 操作代码
    @Setter
    @Getter
    String code;

    // 提示信息
    @Setter
    @Getter
    String message;

    private ServieStatusEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ServieStatusEnum findByCode(String value) {
        for (ServieStatusEnum testEnum : ServieStatusEnum.values()) {
            if (testEnum.getCode().equalsIgnoreCase(value)) {
                return testEnum;
            }
        }
        return null;
    }

    /**
     * 判断是否在枚举类当中
     *
     * @param value
     * @return
     */
    @Override
    public boolean existValidate(String value) {
        if (value == null || "".equals(value)) {
            return false;
        }
        for (ServieStatusEnum testEnum : ServieStatusEnum.values()) {
            if (testEnum.getCode().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}