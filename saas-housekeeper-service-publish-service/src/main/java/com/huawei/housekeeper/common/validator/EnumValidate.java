/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.validator;

/**
 * 功能描述 用于实现枚举类的校验
 *
 * @author jWX1116205
 * @since 2022-01-05
 */
public interface EnumValidate<T> {
    /**
     * 校验枚举值是否存在
     */
    boolean existValidate(T value);
}