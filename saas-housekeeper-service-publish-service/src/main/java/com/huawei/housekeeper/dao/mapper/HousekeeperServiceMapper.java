/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.controller.response.HouseKeeperServiceListVo;
import com.huawei.housekeeper.dao.entity.HousekeeperService;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 服务主表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface HousekeeperServiceMapper extends BaseMapper<HousekeeperService> {
    List<HouseKeeperServiceListVo> getHouseKeeperServicePageByParam(Map<String, Object> params);

    List<HousekeeperService> getHouseKeeperServiceIndexPageByParam(Map<String, Object> params);

    int getHouseKeeperServiceCountByParam(Map<String, Object> params);

    int getHouseKeeperServiceCountIndexByParam(Map<String, Object> params);
}