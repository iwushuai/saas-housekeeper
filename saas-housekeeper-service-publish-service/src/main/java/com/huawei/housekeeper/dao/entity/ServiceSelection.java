/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.huawei.housekeeper.commonutils.entity.BaseEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 选集表
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_service_selection")
public class ServiceSelection extends BaseEntity {
    /**
     * 选项ID
     */
    @TableField("option_id")
    private Long optionId;

    /**
     * SKU_ID
     */
    @TableField("sku_id")
    private Long skuId;
    
    /**
     * 删除标志
     */
    @TableField("delete_flag")
    private String deleteFlag;

    /**
     * 乐观锁
     */
    @TableField("revision")
    private String revision;

}