/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.dao.entity.ServiceOption;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 规格选项表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceOptionMapper extends BaseMapper<ServiceOption> {
    int deleteBySpecificationIds(List<Long> specIds, String updatedBy);

    int insertBatch(List<ServiceOption> records);
}