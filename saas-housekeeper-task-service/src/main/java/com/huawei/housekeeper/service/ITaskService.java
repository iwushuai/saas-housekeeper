/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.commonutils.result.ListRes;
import com.huawei.housekeeper.controller.request.DoTaskDto;
import com.huawei.housekeeper.controller.request.MyTaskDetailDto;
import com.huawei.housekeeper.controller.request.PageQueryMyTaskDto;
import com.huawei.housekeeper.controller.request.PageQueryTaskDto;
import com.huawei.housekeeper.controller.request.TaskDetailDto;
import com.huawei.housekeeper.controller.response.MyTaskDetailVo;
import com.huawei.housekeeper.controller.response.MyTaskListVo;
import com.huawei.housekeeper.controller.response.TaskDetailVo;
import com.huawei.housekeeper.controller.response.TaskPageListVo;

/**
 * 任务表服务类
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
public interface ITaskService {
    /**
     * 查询个人已接任务详情，除了用户名称和电话
     *
     * @param myTaskDetailDto 任务Dto
     * @return 任务详情
     */
    MyTaskDetailVo myTaskDetailDto(MyTaskDetailDto myTaskDetailDto);

    /**
     * 查询个人已接任务
     *
     * @param pageQueryMyTaskDto
     * @return 个人已接任务列表
     */
    ListRes<MyTaskListVo> myTaskListDto(PageQueryMyTaskDto pageQueryMyTaskDto);

    /**
     * 抢单或完成任务
     *
     * @param doTaskDto 任务Dto
     * @return id
     */
    Long updateTask(DoTaskDto doTaskDto);

    /**
     * 任务详情，返回任务详情，除了用户名称和电话
     *
     * @param taskDetailDto 任务表Dto
     * @return 任务表列表
     */
    TaskDetailVo getTaskInfo(TaskDetailDto taskDetailDto);

    /**
     * 雇员任务查询接口
     *
     * @param pageQueryTaskDto 分页请求Dto
     * @return 雇员任务列表
     */
    ListRes<TaskPageListVo> pageQueryTasks(PageQueryTaskDto pageQueryTaskDto);
}