/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.common.enums;

import com.huawei.housekeeper.commonutils.constants.BaseCode;

import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {

    TENANT_MISS_ERROR(440000, "缺失租户标识!"),

    TASK_UPDATE_NOT_PERMISSION_CANCEL(440010, "取消操作，任务的状态必须是已接单！!"),

    TASK_UPDATE_NOT_PERMISSION_FINISHED(440011, "任务完成操作，任务的状态必须是已接单！"),

    TASK_UPDATE_NOT_PERMISSION_GRAP(440012, "抢单操作，任务的状态必须是待抢单！"),

    TASK_UPDATE_NOT_PERMISSION(440013, "任务操作非法!"),

    TASK_ORDER_CANCELED(440014, "订单任务已取消!"),

    TASK_NOT_EXIST(440002, "任务不存在!"),

    TASK_ORDER_EXIST(440003, "订单任务已存在!"),

    EMPLOY_PHONE_NOT_EMPTY(440004, "用户电话必填!"),

    TASK_GRAP_FAILED(440005, "抢单失败!"),

    PARAM_NOT_EMPTY(440009, "参数必填!"),

    MESSAGE_ERROR(440008, "消息接收失败");

    private int code;

    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}