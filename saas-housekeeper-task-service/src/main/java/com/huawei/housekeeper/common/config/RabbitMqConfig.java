/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.common.config;

import com.huawei.housekeeper.commonutils.constants.RabbitMqTopicConfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述 监听来自订单中心发来的消息服务
 *
 * @since 2022-03-03
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 订单消息队列
     *
     * @return Queue 订单消息队列
     */
    @Bean(RabbitMqTopicConfig.QUEUE_TASK)
    public Queue queueOrder() {
        return new Queue(RabbitMqTopicConfig.QUEUE_TASK);
    }

    /**
     * 声明交换机
     *
     * @return 换机
     */
    @Bean(RabbitMqTopicConfig.EXCHANGE_TOPIC_AMQ)
    public Exchange exchangeTopicInform() {
        // 声明了一个Topic类型的交换机，durable是持久化（重启rabbitmq这个交换机不会被自动删除）
        return ExchangeBuilder.topicExchange(RabbitMqTopicConfig.EXCHANGE_TOPIC_AMQ).durable(true).build();
    }

    /**
     * 声明订单队列和交换机绑定关系，并且指定RoutingKey
     *
     * @param queue
     * @param exchange
     * @return 绑定关系
     */
    @Bean
    public Binding orderBindingTopic(@Qualifier(RabbitMqTopicConfig.QUEUE_TASK) Queue queue,
        @Qualifier(RabbitMqTopicConfig.EXCHANGE_TOPIC_AMQ) Exchange exchange) {
        return BindingBuilder.bind(queue)
            .to(exchange)
            .with(RabbitMqTopicConfig.ROUTINGKEY_TASK_ORDER_NEWORDER)
            .noargs();
    }
}