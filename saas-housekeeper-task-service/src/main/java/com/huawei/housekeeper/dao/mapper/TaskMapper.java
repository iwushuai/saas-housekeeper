/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.dao.entity.Task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 任务表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Mapper
public interface TaskMapper extends BaseMapper<Task> {
    /**
     * 工人抢单
     *
     * @param task
     * @return 更新记录数
     */
    int grapTaskOrder(Task task);
}