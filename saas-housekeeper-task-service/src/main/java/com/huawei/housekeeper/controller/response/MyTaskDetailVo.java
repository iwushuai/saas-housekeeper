/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 查询个人已接任务详情
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("任查询个人已接任务详情响应vo")
public class MyTaskDetailVo extends BaseTaskVo {
    @ApiModelProperty("顾客名称")
    private String customerName;

    @ApiModelProperty("电话号码")
    private String customerPhone;
}