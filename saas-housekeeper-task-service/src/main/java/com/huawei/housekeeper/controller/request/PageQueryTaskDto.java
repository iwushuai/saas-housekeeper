/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.huawei.housekeeper.commonutils.request.PageRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;

/**
 * 功能描述 雇员任务查询
 *
 * @since 2022-03-02
 */
@ApiModel("雇员任务查询对象")
@Data
public class PageQueryTaskDto extends PageRequest {
    @Length(max = 32, message = "最大长度:32")
    @ApiModelProperty(value = "雇佣id", required = false)
    private String employeeId;

    @Length(max = 50, message = "最大长度:50")
    @ApiModelProperty(value = "任务状态-accept,cancel,finished", required = false)
    private String taskStatus;

    @Length(max = 255, message = "最大长度:255")
    @ApiModelProperty(value = "顾客名称", required = false)
    private String customerName;

    @Min(value = 1, message = "最小值:1")
    @ApiModelProperty(value = "订单id", required = false)
    private Long orderId;
}