/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import com.huawei.housekeeper.commonutils.result.ListRes;
import com.huawei.housekeeper.commonutils.result.Result;
import com.huawei.housekeeper.controller.request.DoTaskDto;
import com.huawei.housekeeper.controller.request.MyTaskDetailDto;
import com.huawei.housekeeper.controller.request.PageQueryMyTaskDto;
import com.huawei.housekeeper.controller.request.PageQueryTaskDto;
import com.huawei.housekeeper.controller.request.TaskDetailDto;
import com.huawei.housekeeper.controller.response.MyTaskDetailVo;
import com.huawei.housekeeper.controller.response.MyTaskListVo;
import com.huawei.housekeeper.controller.response.TaskDetailVo;
import com.huawei.housekeeper.controller.response.TaskPageListVo;
import com.huawei.housekeeper.service.ITaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 任务表接口
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@RestController
@Validated
@RequestMapping("/housekepper/task")
@Api(tags = "任务大厅接口")
public class TaskController {
    @Autowired
    private ITaskService taskService;

    @PostMapping(value = "/myTask")
    @ApiOperation(value = "查询个人已接任务详情，显示用户名称和电话")
    public Result<MyTaskDetailVo> myTask(@Valid @RequestBody MyTaskDetailDto myTaskDetailDto,
        @RequestHeader(defaultValue = "Authorization", name = HttpHeaders.AUTHORIZATION) String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(taskService.myTaskDetailDto(myTaskDetailDto));
    }

    @PostMapping(value = "/myTasks")
    @ApiOperation(value = "查询个人已接任务")
    public Result<ListRes<MyTaskListVo>> myTasks(@RequestBody PageQueryMyTaskDto pageQueryMyTaskDto,
        @RequestHeader(defaultValue = "Authorization", name = HttpHeaders.AUTHORIZATION) String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(taskService.myTaskListDto(pageQueryMyTaskDto));
    }

    @PostMapping(value = "/doTask")
    @ApiOperation(value = "抢单或完成任务")
    public Result<Long> doTask(@Valid @RequestBody DoTaskDto doTaskDto,
        @RequestHeader(defaultValue = "Authorization", name = HttpHeaders.AUTHORIZATION) String authorization,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(taskService.updateTask(doTaskDto));
    }

    @PostMapping(value = "/taskDetail")
    @ApiOperation(value = "任务详情，返回任务详情，除了用户名称和电话")
    public Result<TaskDetailVo> taskDetail(@Valid @RequestBody TaskDetailDto taskDetailDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(taskService.getTaskInfo(taskDetailDto));
    }

    @PostMapping(value = "/tasks")
    @ApiOperation(value = "雇员任务查询接口")
    public Result<ListRes<TaskPageListVo>> pageQueryTasks(@Valid @RequestBody PageQueryTaskDto pageQueryTaskDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(taskService.pageQueryTasks(pageQueryTaskDto));
    }
}